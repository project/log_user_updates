<?php

/**
 * @file
 * {@inheritdoc}
 */

/**
 * Implements hook_views_data().
 */
function log_user_updates_views_data() {
    $data = array();
    $data['user_update_history']['table']['group'] = t('User Log History');

    $data['user_update_history']['table']['base'] = array(
        // This is the identifier field for the view.
        'field' => 'id',
        'title' => t('User Log History'),
        'help' => t('User Log History'),
        'weight' => -10,
        'description' => 'Updated user history',
    );

    $data['user_update_history']['table']['join'] = array(
        'users' => array(
            'left_field' => 'uid',
            'field' => 'updated_uid',
        ),
    );
    $data['user_update_history']['table']['join'] = array(
        'users' => array(
            'left_field' => 'uid',
            'field' => 'updated_by_uid',
        ),
    );

    $data['user_update_history']['updated_uid'] = array(
        'title' => t('Updated user id'),
        'help' => t('This user was updated'),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE,
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'argument' => array(
            'handler' => 'views_handler_argument',
        ),
        'relationship' => array(
            'base' => 'users',
            'base field' => 'uid',
            'handler' => 'views_handler_relationship',
            'label' => t('Updated user'),
        ),
    );
    $data['user_update_history']['updated_by_uid'] = array(
        'title' => t('User Id who Updated'),
        'help' => t('By this User other user account/profile was updated'),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE,
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'argument' => array(
            'handler' => 'views_handler_argument',
        ),
        'relationship' => array(
            'base' => 'users',
            'base field' => 'uid',
            'handler' => 'views_handler_relationship',
            'label' => t('Updated By'),
        ),
    );
    $data['user_update_history']['updated'] = array(
        'title' => t('Updated Time'),
        'help' => t('Updated time of User account'),
        'field' => array(
            'handler' => 'views_handler_field_date',
            'click sortable' => TRUE,
            'is date' => TRUE,
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'argument' => array(
            'handler' => 'views_handler_argument',
        ),
    );

    return $data;
}

/*
 * Implements hook_views_default_views to create default view
 */
function log_user_updates_views_default_views() {

    $view = new view();
    $view->name = 'user_log_history';
    $view->description = '';
    $view->tag = 'default';
    $view->base_table = 'user_update_history';
    $view->human_name = 'User Log History';
    $view->core = 7;
    $view->api_version = '3.0';
    $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

    /* Display: Master */
    $handler = $view->new_display('default', 'Master', 'default');
    $handler->display->display_options['title'] = 'User Log History';
    $handler->display->display_options['use_more_always'] = FALSE;
    $handler->display->display_options['access']['type'] = 'none';
    $handler->display->display_options['cache']['type'] = 'none';
    $handler->display->display_options['query']['type'] = 'views_query';
    $handler->display->display_options['exposed_form']['type'] = 'basic';
    $handler->display->display_options['pager']['type'] = 'full';
    $handler->display->display_options['pager']['options']['items_per_page'] = '10';
    $handler->display->display_options['style_plugin'] = 'table';
    $handler->display->display_options['style_options']['columns'] = array(
        'updated_uid' => 'updated_uid',
        'updated' => 'updated',
        'updated_by_uid' => 'updated_by_uid',
    );
    $handler->display->display_options['style_options']['default'] = '-1';
    $handler->display->display_options['style_options']['info'] = array(
        'updated_uid' => array(
            'sortable' => 0,
            'default_sort_order' => 'asc',
            'align' => '',
            'separator' => '',
            'empty_column' => 0,
        ),
        'updated' => array(
            'sortable' => 0,
            'default_sort_order' => 'asc',
            'align' => '',
            'separator' => '',
            'empty_column' => 0,
        ),
        'updated_by_uid' => array(
            'sortable' => 0,
            'default_sort_order' => 'asc',
            'align' => '',
            'separator' => '',
            'empty_column' => 0,
        ),
    );
    /* Relationship: User Log History: Updated user id */
    $handler->display->display_options['relationships']['updated_uid']['id'] = 'updated_uid';
    $handler->display->display_options['relationships']['updated_uid']['table'] = 'user_update_history';
    $handler->display->display_options['relationships']['updated_uid']['field'] = 'updated_uid';
    /* Relationship: User Log History: User Id who Updated */
    $handler->display->display_options['relationships']['updated_by_uid']['id'] = 'updated_by_uid';
    $handler->display->display_options['relationships']['updated_by_uid']['table'] = 'user_update_history';
    $handler->display->display_options['relationships']['updated_by_uid']['field'] = 'updated_by_uid';
    /* Field: User: Name */
    $handler->display->display_options['fields']['name']['id'] = 'name';
    $handler->display->display_options['fields']['name']['table'] = 'users';
    $handler->display->display_options['fields']['name']['field'] = 'name';
    $handler->display->display_options['fields']['name']['relationship'] = 'updated_uid';
    $handler->display->display_options['fields']['name']['label'] = 'Updated User';
    /* Field: User: Name */
    $handler->display->display_options['fields']['name_1']['id'] = 'name_1';
    $handler->display->display_options['fields']['name_1']['table'] = 'users';
    $handler->display->display_options['fields']['name_1']['field'] = 'name';
    $handler->display->display_options['fields']['name_1']['relationship'] = 'updated_by_uid';
    $handler->display->display_options['fields']['name_1']['label'] = 'Updated By';
    /* Field: User Log History: Updated Time */
    $handler->display->display_options['fields']['updated']['id'] = 'updated';
    $handler->display->display_options['fields']['updated']['table'] = 'user_update_history';
    $handler->display->display_options['fields']['updated']['field'] = 'updated';
    $handler->display->display_options['fields']['updated']['date_format'] = 'short';
    $handler->display->display_options['fields']['updated']['second_date_format'] = 'long';
    /* Sort criterion: User Log History: User Id who Updated */
    $handler->display->display_options['sorts']['updated_by_uid']['id'] = 'updated_by_uid';
    $handler->display->display_options['sorts']['updated_by_uid']['table'] = 'user_update_history';
    $handler->display->display_options['sorts']['updated_by_uid']['field'] = 'updated_by_uid';
    $handler->display->display_options['sorts']['updated_by_uid']['order'] = 'DESC';

    /* Display: User log history */
    $handler = $view->new_display('page', 'User log history', 'page');
    $handler->display->display_options['path'] = 'user-log-history';



    $views[$view->name] = $view;
    return $views;
}
